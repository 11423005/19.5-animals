﻿#include <iostream>
#include <vector>

class Animal
{
public:
    virtual void Voice() const 
    {
        std::cout << "Generic animal sound" << std::endl;
    }
};

class Dog : public Animal 
{
public:
    void Voice() const override 
    {
        std::cout << "Woof" << std::endl;
    }
};

class Panda : public Animal 
{
public:
    void Voice() const override 
    {
        std::cout << "Munch munch munch" << std::endl;
    }
};

class Fox : public Animal 
{
public:
    void Voice() const override 
    {
        std::cout << "Ring-ding-ding-ding-dingeringeding" << std::endl;
    }
};

int main() 
{
    std::vector<Animal*> animals;
    animals.push_back(new Dog());
    animals.push_back(new Panda());
    animals.push_back(new Fox());

    for (const auto& animal : animals) 
    {
        animal->Voice();
    }

    // Очистка памяти
    for (auto& animal : animals) 
    {
        delete animal;
    }
    animals.clear();

    return 0;
}